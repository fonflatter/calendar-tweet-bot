const fs = require('fs');
const path = require('path');
const ICAL = require('ical.js');
const Twitter = require('twitter');

const secrets = JSON.parse(fs.readFileSync('.twitter-secrets.json'));
const client = new Twitter(secrets);

const now = ICAL.Time.now();
const isSameDate = (firstDate, secondDate) => (firstDate.year === secondDate.year) && (firstDate.month === secondDate.month) && (firstDate.day === secondDate.day);
const isToday = event => isSameDate(event.startDate, now);

if (!process.env.ICS_FILE_PATH) {
  throw new Error('Please set the environment variable ICS_FILE_PATH!');
}

const calendarData = fs.readFileSync(path.join(process.env.ICS_FILE_PATH, `${now.year}.ics`));
const calendar = ICAL.parse(calendarData.toString());
const calendarComponent = new ICAL.Component(calendar);
const events = calendarComponent.getAllSubcomponents('vevent').map(eventData => new ICAL.Event(eventData));

const formatTweetText = eventNames => {
  if (!eventNames.length) {
    return null;
  }

  let text = 'Heute ist ';

  if (eventNames.length === 1) {
    text += eventNames[0];
  } else {
    text += eventNames.slice(0, -2)
      .concat(eventNames.slice(-2).join(' und '))
      .join(', ');
  }

  text += '. #Fredkalender\nhttps://www.fonflatter.de/kalender/';
  return text;
}

const lastTweet = JSON.parse(fs.readFileSync('.last-tweet.json'));
const lastTweetDateTime = ICAL.Time.fromString(lastTweet.dateTime);

if (isSameDate(now, lastTweetDateTime)) {
  throw new Error(`${lastTweet.url} exists already! (${lastTweetDateTime})`);
}

const tweet = {
  dateTime: now.toString(),
  text: formatTweetText(events.filter(isToday).map(event => event.summary))
}

if (!tweet.text) {
  throw new Error('No events available!')
}

const status = tweet.text;
client.post('statuses/update', { status})
  .then((data) => {
    tweet.url = `https://twitter.com/bastianmelnyk/status/${data.id_str}`
    fs.writeFileSync('.last-tweet.json', JSON.stringify(tweet));
    console.log(tweet)
  })
  .catch(console.error);
